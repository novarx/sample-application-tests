package ch.novarx.sample.apptest;

import com.github.tomakehurst.wiremock.junit5.WireMockTest;
import io.restassured.specification.RequestSpecification;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static io.restassured.RestAssured.given;
import static java.lang.System.getenv;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.*;

class AppTest {
    @Nested
    @WireMockTest(httpPort = 9090)
    class KotlinBackend {
        RequestSpecification when = given().baseUri(getenv("APPTEST_BASE_URI")).when();

        @Test
        void testPing() {
            stubFor(get("/pong").willReturn(okJson(
                    "[\"not-bacon\", \"ipsum\"]"
            )));

            when.get("/ping")
                    .prettyPeek()
                    .then()
                    .statusCode(200)
                    .body("ping", not(is(emptyOrNullString())))
                    .body("pong", allOf(
                            hasItem("not-bacon"),
                            hasItem("ipsum")
                    ));

            verify(1, getRequestedFor(urlEqualTo("/pong")));
        }

        @Test
        void testPong() {
            when.get("/pong")
                    .prettyPeek()
                    .then()
                    .statusCode(200)
                    .body("$", allOf(
                            hasItem("lorem"),
                            hasItem("ipsum")
                    ));
        }
    }
}
