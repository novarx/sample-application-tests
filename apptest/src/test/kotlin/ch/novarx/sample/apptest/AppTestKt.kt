package ch.novarx.sample.apptest

import com.github.tomakehurst.wiremock.client.WireMock.*
import com.github.tomakehurst.wiremock.junit5.WireMockTest
import io.restassured.module.kotlin.extensions.Given
import io.restassured.module.kotlin.extensions.Then
import io.restassured.module.kotlin.extensions.When
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.Test
import java.lang.System.getenv

@WireMockTest(httpPort = 9090)
class AppTestKt {
    @Test
    fun testPing() {
        stubFor(
            get("/pong").willReturn(
                okJson(
                    "[\"not-bacon\", \"ipsum\"]"
                )
            )
        )

        Given {
            baseUri(getenv("APPTEST_BASE_URI"))
        } When {
            get("/ping").prettyPeek()
        } Then {
            statusCode(200)
            body("ping", not(emptyOrNullString()))
            body(
                "pong", allOf(
                    hasItem("not-bacon"),
                    hasItem("ipsum")
                )
            )
        }

        verify(1, getRequestedFor(urlEqualTo("/pong")))
    }
}