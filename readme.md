# Example application test

This project shows one way to test rest-endpoints, using [REST-assured](https://rest-assured.io/).

## Overview

The system-under-test (sut) is `application-kotlin`, which is dependent on another REST service `application-python`
To automatically test the `application-kotlin` on it's own, meaning not having to rely on `application-python`,
it is wise to "mock" all subsystems. Due to the ambiguity of test type names, that kind of test will be called
_application test_ or _AppTest_ later on.

The following diagram shows this basic architecture of the test setup.
Note that...
- the green _apptest_ package contains the needed components to run the AppTests
- the blue _application-kotlin_ package is the actual system-under-test
- the grey _application-python_ package is a subsystem, which will not be tested


```plantuml
@startuml
package "AppTest" <<TEST>> as test_setup #lightgreen {
    agent AppTest
    agent Wiremock
    AppTest --> Wiremock : start
}

package "application-kotlin" <<SUT>> as kotlin #lightblue {
    card PingResource
    card PongAdapter
}

package "application-python" as python #lightgrey {
    card PongResource
}

AppTest -> PingResource : GET /ping
Wiremock <- PongAdapter

PingResource --> PongAdapter
PongAdapter ..> PongResource: "  is mocked in AppTests "

@enduml
```

## Run application tests

See [.gitlab-ci.yml](./.gitlab-ci.yml) for build, test and execute usecases or run following commands:

```shell
# start system-under-test
docker run \
       --rm --detach --pull always -p8080:8080 \
       registry.gitlab.com/nvax/examples/sample-application-tests/application-kotlin

# navigate to apptest module
cd ./apptest
# run application tests
mvn verify
```
