package ch.novarx.sample

import io.quarkus.test.InjectMock
import io.quarkus.test.junit.QuarkusTest
import io.restassured.RestAssured.given
import org.eclipse.microprofile.rest.client.inject.RestClient
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.Matchers.*
import org.junit.jupiter.api.Test
import org.mockito.Mockito


@QuarkusTest
class PingResourceTest {

    @InjectMock
    @RestClient
    lateinit var pongAdapter: PongAdapter

    @Test
    fun testPongEndpoint() {
        Mockito.`when`(pongAdapter.pong()).thenReturn(
            setOf(
                "lorem", "ipsum"
            )
        )

        given()
            .`when`().get("/ping")
            .then()
            .statusCode(200)
            .body("ping", not(`is`(emptyOrNullString())))
            .body(
                "pong", allOf(
                    hasItem("lorem"),
                    hasItem("ipsum"),
                )
            )
    }
}