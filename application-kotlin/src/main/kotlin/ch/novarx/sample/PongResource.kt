package ch.novarx.sample

import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType.APPLICATION_JSON
import kotlin.math.abs
import kotlin.random.Random.Default.nextInt

@Path("/pong")
class PongResource {
    @GET
    @Produces(APPLICATION_JSON)
    fun ping() = listOf("lorem", "ipsum", abs(nextInt()))
}