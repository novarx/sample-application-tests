package ch.novarx.sample

import jakarta.inject.Inject
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.MediaType.APPLICATION_JSON
import org.eclipse.microprofile.rest.client.inject.RestClient
import java.util.UUID.randomUUID

@Path("/ping")
class PingResource {

    @Inject
    @RestClient
    lateinit var pongAdapter: PongAdapter

    @GET
    @Produces(APPLICATION_JSON)
    fun ping() = object {
        val ping = randomUUID()
        val pong = pongAdapter.pong()
    }
}