package ch.novarx.sample

import jakarta.enterprise.context.ApplicationScoped
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.PathParam
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient


@Path("/pong")
@ApplicationScoped
@RegisterRestClient(configKey = "pong-adapter")
interface PongAdapter {
    @GET
    fun pong(): Set<String?>?

    @GET
    @Path("/{id}")
    fun pongById(@PathParam("id") id: String): String?
}